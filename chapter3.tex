\chapter{Characterization of an optimal solution to Wiener Max-QAP}\label{Chap3}
In this chapter, we talk about a \textit{decomposition property} of the Wiener Max-QAP and apply the decomposition property to partially characterize the structure of optimal permutations.

\section{Decomposition property of the Wiener Max-QAP}\label{DecompProp}
In this section, following \citep{ccela2011wiener}, we prove that the problem of finding a permutation that maximizes the objective function in \eqref{maxwiener} can be decomposed into two separate optimization problems which can be solved independently from each other.

Note the following important remark which follows from both the notions of set partition and bijective maps on a finite set.

\begin{rem}\label{BijecIndu}
Let $X=\{1,2,\ldots, n\}$, $n\geq 2$. Let $I\subset X$ with $|I|=k\geq 1$. Let $J=\{1,2,\ldots, k\}$ and $\pi$ be a permutation of $X$. Then there are bijective maps $\sigma: J \to I$ and $\tau: J^C \to I^C$ such that $\sigma(j)=\pi(j)$ for all $j\in J$ and  $\tau(j)=\pi(j)$ for all $j\in J^C$.
\end{rem}
%%%%%%%%%%%%

Consider $n$ non-negative integers $\alpha_1 \leq \alpha_2 \leq \cdots \leq \alpha_n$ and $n$ integers $\beta_1 \leq \beta_2 \leq \cdots \leq \beta_n$. Let $I\subset \{1,2,\ldots,n\}$ be a fixed subset of $k$ elements and $J=\{1,2,\ldots,k\}$. We construct a \textit{subdivided version} of the Wiener Max-QAP by only considering permutations $\pi$ that map $J$ to the fixed subset $I$ and that consequently map $J^C$ to $I^C$. Thus, from Remark \ref{BijecIndu} we have two bijective functions induced by $\pi$. Using the fact that $J$ and $J^C$ form a partition of $\{1,2,\ldots,n\}$, we can rewrite Equation \eqref{maxwiener} as
\begin{align}
\label{dec1} 
Z_{\pi}(A,B)=& \sum_{i\in J} \sum_{j\in J} \alpha_{\pi(i)} \alpha_{\pi(j)}|\beta_i - \beta_j|\\
\label{dec2} 
+ & \sum_{i\in J} \sum_{j\notin J} \alpha_{\pi(i)} \alpha_{\pi(j)}|\beta_i - \beta_j|\\
\label{dec3} 
+ & \sum_{i\notin J} \sum_{j\in J} \alpha_{\pi(i)} \alpha_{\pi(j)}|\beta_i - \beta_j|\\
\label{dec4} 
+ & \sum_{i\notin J} \sum_{j\notin J} \alpha_{\pi(i)} \alpha_{\pi(j)}|\beta_i - \beta_j|\,.
\end{align}

Let $\sigma$ be the bijective function that maps $J$ to $I$ and $\tau$ the bijective function that maps $J^C$ to $I^C$. That is
\begin{align*}
\sigma: J \to I \quad \text{with}\quad \sigma(j)=\pi(j), \quad \text{and}\quad \tau: J^C \to I^C \quad \text{with}\quad \tau(j)=\pi(j)\,.
\end{align*}

Let us denote the double sums in \eqref{dec1}, \eqref{dec2}, \eqref{dec3} and \eqref{dec4} by $Z_1, Z_2, Z_3$ and $Z_4$, respectively. Using the bijective functions $\sigma$ and $\tau$, they can be rewritten in the following way:
\begin{align}
\label{dec1p} 
Z_1=& \sum_{i\in J} \sum_{j\in J} \alpha_{\sigma(i)} \alpha_{\sigma(j)}|\beta_i - \beta_j|\,,\\
\label{dec2p} 
Z_2= & \sum_{i\in J} \sum_{j\notin J} \alpha_{\sigma(i)} \alpha_{\tau(j)}|\beta_i - \beta_j|\,,\\
\label{dec3p} 
Z_3= & \sum_{i\notin J} \sum_{j\in J} \alpha_{\tau(i)} \alpha_{\sigma(j)}|\beta_i - \beta_j|\,,\\
\label{dec4p} 
Z_4=& \sum_{i\notin J} \sum_{j\notin J} \alpha_{\tau(i)} \alpha_{\tau(j)}|\beta_i - \beta_j|\,.
\end{align}

We notice that $Z_2=Z_3$. Now, let us consider the term $Z_2$. We have $i\in J$ and $j\notin J$ which means $i\leq k$ and $k < j$. Thus $\beta_i \leq \beta_j$. So we can write $|\beta_i - \beta_j|$ simply as $\beta_j - \beta_i$. Also we can separate the indices $i$ and $j$ by rewriting $\beta_j - \beta_i$ as $(\beta_j - \beta_k) + (\beta_k - \beta_i)$.

Thus, we can rewrite $Z_2$ as the sum of two terms as follows:
\begin{align*}
\sum_{i\in J} \sum_{j\notin J} \alpha_{\sigma(i)} &\alpha_{\tau(j)}(\beta_j - \beta_k)+\sum_{i\in J} \sum_{j\notin J} \alpha_{\sigma(i)} \alpha_{\tau(j)}(\beta_k - \beta_i)\\
&=\sum_{j\notin J}\alpha_{\tau(j)} \Bigg(\sum_{i\in J} \alpha_{\sigma(i)}\Bigg)(\beta_j - \beta_k)+ \sum_{i\in J}\alpha_{\sigma(i)} \Bigg(\sum_{j\notin J}\alpha_{\tau(j)}\Bigg)(\beta_k - \beta_i)\,.
\end{align*}

Furthermore, $\displaystyle \sum_{j\notin J}\alpha_{\tau(j)}=\sum_{l\notin I}\alpha_l$ since $\tau(j) \in I^C$ for all $j\in J^C$. Likewise $\displaystyle \sum_{i\in J} \alpha_{\sigma(i)}=\sum_{l\in I} \alpha_l$ since $\sigma(i) \in I$ for all $i\in J$.

Setting $\displaystyle X=\sum_{l\in I} \alpha_l$ and $\displaystyle Y=\sum_{l\notin I} \alpha_l$, the objective function $Z_{\pi}(A,B)$ becomes
\begin{align}
\label{decf1} 
Z_{\pi}(A,B)=& \sum_{i\in J} \sum_{j\in J} \alpha_{\sigma(i)} \alpha_{\sigma(j)}|\beta_i - \beta_j|\\
\label{decf23} 
+ & 2 \Bigg(\sum_{i\in J}\alpha_{\sigma(i)} Y(\beta_k - \beta_i) +  \sum_{j\notin J}\alpha_{\tau(j)} X(\beta_j - \beta_k)\Bigg)\\
\label{decf4} 
+& \sum_{i\notin J} \sum_{j\notin J} \alpha_{\tau(i)} \alpha_{\tau(j)}|\beta_i - \beta_j|\,.
\end{align}

We notice that in the resulting sums in \eqref{decf1}, \eqref{decf23} and \eqref{decf4}, every single term either depends on the bijective function $\sigma$ or on the bijective function $\tau$, but none of them depends on both bijective functions. If we regroup all the terms in $Z_{\pi}(A,B)$ that depend only on the function $\sigma$, we get
\begin{align}
\label{sep1} 
\sum_{i\in J} \sum_{j\in J} \alpha_{\sigma(i)} \alpha_{\sigma(j)}|\beta_i - \beta_j| + 2 \sum_{i\in J}\alpha_{\sigma(i)} Y(\beta_k - \beta_i)\,.
\end{align}

Likewise, if we collect all the terms in $Z_{\pi}(A,B)$ that depend only on the function $\tau$, we obtain
\begin{align}
\label{sep2} 
\sum_{i\notin J} \sum_{j\notin J} \alpha_{\tau(i)} \alpha_{\tau(j)}|\beta_i - \beta_j| + 2 \sum_{j\notin J}\alpha_{\tau(j)} X(\beta_j - \beta_k)\,.
\end{align}

\subsection{Decomposition property}
Let $n\geq 2$. This subdivided version QAP is as we are given a set $W=\{\alpha_1,\alpha_2, \dots, \alpha_n\}$ of $n$ weights with $\alpha_1\leq \alpha_2\leq \cdots \leq \alpha_n$ and a $\beta$-sequence $\beta_1\leq \beta_2\leq \cdots \leq \beta_k \leq \beta_{k+1}\leq \cdots \leq \beta_n$, and we reduce the problem from $n$-dimensional to a lower dimensional problem in the following way:
\begin{itemize}
\item Let us consider the expression in \eqref{sep1}. We pick any $k$ weights from the set $W$ and we let $I$ be the set containing the indices of these elements (for example, if we pick $\alpha_2,\alpha_5$ and $\alpha_6$ then $I=\{2,5,6\}$). Next, we build a new weight $Y$ from the remaining elements of $W$ by setting $\displaystyle Y=\sum_{i\notin I} \alpha_{i}$. Finally, we build the 1-D distance matrix around the first $k$ points $\beta_1,\beta_2, \ldots, \beta_k$ by assigning the values $\alpha_i$, $i\in I$ to the points $\beta_1,\beta_2, \ldots, \beta_k$ with the additional restriction that the point $\beta_k$ is duplicated and $Y$ is assigned to $\beta_k$. Thus, we notice that the objective function in \eqref{sep1} becomes a $(k+1)$-dimensional problem with elements $\alpha_i, i\in I$, and $Y$.
\item Let us consider the expression in \eqref{sep2}. We build a new weight $X$ by setting $\displaystyle X=\sum_{i\in I} \alpha_{i}$, and we construct the 1-D distance matrix around the remaining $n-k$ points $\beta_{k+1},\beta_{k+2}, \ldots, \beta_n$ by assigning the values $\alpha_i$, $i\in I^C$ to the points $\beta_{k+1},\beta_{k+2}, \ldots, \beta_n$ and the value $X$ to $\beta_k$. Thus, we notice that the objective function in \eqref{sep2} becomes an $(n-k+1)$-dimensional problem with elements $\alpha_i, i\in I^C$, and $X$.
\end{itemize}

Therefore, we observe that finding the optimal bijective function $\sigma$ and finding the optimal bijective function $\tau$ are two separate optimization problems, and furthermore, we can solve them independently from each other. This is what we mean by \textit{decomposition property} of the Wiener Max-QAP according to \citep{ccela2011wiener}.

The $(k+1)$-dimensional version with optimal bijective function $\sigma$ and the $(n-k+1)$-dimensional version with optimal bijective function $\tau$ are called the \textit{subdivided version} of the Wiener Max-QAP.

Note that using the decomposition property one can construct the subdivided version of any dimension with respect to either the bijective function $\sigma$ or the bijective function $\tau$.

In the next section, we apply the decomposition property to derive a partial characterization of optimal solutions.

\section{Partial characterization of the structure of optimal solutions}
\begin{defn}
Let $n\geq 1$ be an integer. A permutation $\Big(a_{\pi(1)},a_{\pi(2)},\ldots,a_{\pi(n)}\Big)$ of the finite sequence of real numbers $a_1,a_2,\ldots,a_n$ is said to be \textit{V-shaped} if there exists some $1\leq l \leq n$ such that
\begin{align*}
a_{\pi(1)}\geq a_{\pi(2)}\geq \cdots \geq a_{\pi(l-1)}\geq a_{\pi(l)} \quad \text{and} \quad a_{\pi(l)}\leq a_{\pi(l+1)}\leq \cdots \leq a_{\pi(n)}\,.
\end{align*}
More precisely, a V-shaped permutation is non-increasing up to a certain point, and then non-decreasing. Note that a non-increasing or a non-decreasing permutation is also considered as V-shaped.
\end{defn} 

\begin{exa}
The following permutations of the sequence $1,2,3,4$ are V-shaped:
\begin{align*}
(4,3,1,2), \quad (1,2,3,4), \quad (4,2,1,3), \quad (3,1,2,4),
\end{align*}
but the following are not V-shaped:
\begin{align*}
(1,2,4,3), \quad (4,1,3,2), \quad (4,2,3,1), \quad (3,4,1,2).
\end{align*}
\end{exa}

\begin{rem}
A V-shaped permutation with both non-increasing and non-decreasing parts has only one local minimum, which occurs at $\pi(l)$ with $a_{\pi(l-1)}\geq a_{\pi(l)}$ and $a_{\pi(l)}\leq a_{\pi(l+1)}$. Also, a permutation which is not V-shaped has at least one local maximum at a point different from its endpoints.
\end{rem}

\begin{thm}\label{thm:V-shaped}
\citep{ccela2011wiener} The Wiener Max-QAP has a V-shaped solution.
\end{thm}

\begin{proof}
Consider a solution to the Wiener Max-QAP. This solution is induced by an optimal permutation $\pi$. We argue by contradiction. Suppose that the optimal permutation $\pi$ has a local maximum at $k$ which means that we have both $\alpha_{\pi(k-1)}\leq \alpha_{\pi(k)}$ and $\alpha_{\pi(k)}\geq \alpha_{\pi(k+1)}$. 

Consider the values $\alpha_{\pi(k-1)}$, $\alpha_{\pi(k)}$ and $\alpha_{\pi(k+1)}$. Denote $\displaystyle L=\sum_{i=1}^{k-2}\alpha_{\pi(i)}$ and  $\displaystyle R=\sum_{i=k+2}^n \alpha_{\pi(i)}$. For simplicity, we may assume that $\alpha_{\pi(k)}$ differs from $\alpha_{\pi(k-1)}$ and $\alpha_{\pi(k+1)}$, and that the points $\beta_{\pi(k-1)},\beta_{\pi(k)}$ and $\beta_{\pi(k+1)}$ are pairwise distinct. In fact, if $\alpha_{\pi(k)}$ equals $\alpha_{\pi(k-1)}$ or $\alpha_{\pi(k+1)}$, or any two of the points $\beta_{\pi(k-1)},\beta_{\pi(k)}$ and $\beta_{\pi(k+1)}$ are the same, then the permutation obtained by switching the positions of these two values will yield the same value of the objective function.

Using the decomposition property discussed in Section \ref{Chap1} of this chapter, we have the following:
\begin{itemize}
\item  $\pi$ induces with respect to the bijective function $\tau$, an optimal solution to the $(n-k+3)$-dimensional version which consists of assigning the $n-k+3$ values $L,\alpha_{\pi(k-1)},\alpha_{\pi(k)},\alpha_{\pi(k+1)},\ldots,\alpha_{\pi(n)}$ to the $n-k+3$ points $\beta_{k-1},\beta_{k-1},\beta_k,\beta_{k+1},\ldots,\beta_{n}$ ($\beta_{k-1}$ is the duplicated point).
\item Considering this $(n-k+3)$-dimensional version, $\pi$ induces with respect to the bijective function $\sigma$ an optimal solution to the $5$-dimensional version which consists of assigning the $5$ values $L,\alpha_{\pi(k-1)},\alpha_{\pi(k)},\alpha_{\pi(k+1)},R$ to the five points $\beta_{k-1},\beta_{k-1},\beta_k,\beta_{k+1},\beta_{k+1}$ ($\beta_{k+1}$ is the duplicated point).
\end{itemize}

The optimal value corresponding to this $5$-dimensional assignment is found in the configuration where
\begin{align}\label{alphakkm1}
L,\quad \alpha_{\pi(k-1)},\quad  \alpha_{\pi(k)},\quad \alpha_{\pi(k+1)},\quad \text{and}\quad R
\end{align}
are assigned to the points
\begin{align*}
\beta_{k-1},\quad \beta_{k-1},\quad \beta_k,\quad \beta_{k+1},\quad \text{and} \quad \beta_{k+1}\,,
\end{align*}
respectively, and given by
\begin{align*} 
&H_1:=\Big(L\alpha_{\pi(k)}(\beta_k - \beta_{k-1})+L\alpha_{\pi(k+1)}(\beta_{k+1}-\beta_{k-1})+LR(\beta_{k+1} - \beta_{k-1})\Big) \\ 
&+\Big(\alpha_{\pi(k-1)}\alpha_{\pi(k)}(\beta_k - \beta_{k-1})+\alpha_{\pi(k-1)}\alpha_{\pi(k+1)}(\beta_{k+1} - \beta_{k-1})+\alpha_{\pi(k-1)}R(\beta_{k+1} - \beta_{k-1})\Big) \\
&+\Big(\alpha_{\pi(k)}L(\beta_k - \beta_{k-1})+\alpha_{\pi(k)}\alpha_{\pi(k-1)}(\beta_k - \beta_{k-1})+\alpha_{\pi(k)}\alpha_{\pi(k+1)}(\beta_{k+1} - \beta_k)+\alpha_{\pi(k)}R(\beta_{k+1} - \beta_k)\Big)\\[6pt]
&+\Big(\alpha_{\pi(k+1)}L(\beta_{k+1} - \beta_{k-1})+\alpha_{\pi(k+1)}\alpha_{\pi(k-1)}(\beta_{k+1} - \beta_{k-1})+\alpha_{\pi(k+1)}\alpha_{\pi(k)}(\beta_{k+1} - \beta_k)\Big) \\
&+\Big(RL(\beta_{k+1} - \beta_{k-1})+R\alpha_{\pi(k-1)}(\beta_{k+1} - \beta_{k-1})+R\alpha_{\pi(k)}(\beta_{k+1} - \beta_k)\Big) \,.
\end{align*}

Note that since $H_1$ is optimal (the maximum value), any other assignment cannot increase its value.

Now let us assign $\alpha_{\pi(k-1)}$ to $\beta_k$ and $\alpha_{\pi(k)}$ to $\beta_{k-1}$ by switching their position in \eqref{alphakkm1}. Then we get a new value for the objective function, and the difference between $H_1$ and this new value is given by
\begin{align*}
&H_2:=\Big(L(\alpha_{\pi(k)}-\alpha_{\pi(k-1)})(\beta_k - \beta_{k-1})+(\alpha_{\pi(k-1)}-\alpha_{\pi(k)})\alpha_{\pi(k+1)}(\beta_{k+1} - \beta_{k-1})\Big)\\
&+\Big((\alpha_{\pi(k-1)}-\alpha_{\pi(k)})R(\beta_{k+1} - \beta_{k-1})\Big)\\
&+\Big((\alpha_{\pi(k)}-\alpha_{\pi(k-1)})L(\beta_k - \beta_{k-1})+(\alpha_{\pi(k)}-\alpha_{\pi(k-1)})\alpha_{\pi(k+1)}(\beta_{k+1} - \beta_k)\Big)\\[6pt]
&+\Big((\alpha_{\pi(k)}-\alpha_{\pi(k-1)})R(\beta_{k+1} - \beta_k)\Big)\\
&+\Big(\alpha_{\pi(k+1)}(\alpha_{\pi(k-1)}-\alpha_{\pi(k)})(\beta_{k+1}-\beta_{k-1})+\alpha_{\pi(k+1)}(\alpha_{\pi(k)}-\alpha_{\pi(k-1)})(\beta_{k+1} - \beta_k)\Big)\\
&+\Big(R(\alpha_{\pi(k-1)}-\alpha_{\pi(k)})(\beta_{k+1} - \beta_{k-1})+R(\alpha_{\pi(k)}-\alpha_{\pi(k-1)})(\beta_{k+1} - \beta_k)\Big)\,,
\end{align*}
which reduces to
\begin{align*}
H_2=:(\alpha_{\pi(k)}-\alpha_{\pi(k-1)})(\beta_k - \beta_{k-1})(2L-2R-2\alpha_{\pi(k+1)})\,.
\end{align*}

This difference $H_2$ between the corresponding two objective values must be non-negative because we have subtracted the new value from the optimal (maximum) value $H_1$. So we have
\begin{align}\label{diff1}
(\alpha_{\pi(k)}-\alpha_{\pi(k-1)})(\beta_k - \beta_{k-1})(L-R-\alpha_{\pi(k+1)})\geq 0\,.
\end{align}

Likewise, we can also switch the positions of $\alpha_{\pi(k)}$ and $\alpha_{\pi(k+1)}$ in \eqref{alphakkm1}. After doing that, the difference between the two objective values gives
\begin{align}\label{diff2}
(\alpha_{\pi(k)}-\alpha_{\pi(k-1)})(\beta_{k+1} - \beta_k)(-L+R-\alpha_{\pi(k-1)})\geq 0\,.
\end{align}

Since we have both $\alpha_{\pi(k)}>\alpha_{\pi(k-1)}$ and $\beta_k > \beta_{k-1}$, Inequality \eqref{diff1} implies $L-R-\alpha_{\pi(k+1)}\geq 0$. Also since $\alpha_{\pi(k)}>\alpha_{\pi(k-1)}$ and $\beta_{k+1}> \beta_k$, Inequality \eqref{diff2} implies $-L+R-\alpha_{\pi(k-1)}\geq 0$. Summing these two inequalities, we get
\begin{align*}
\alpha_{\pi(k-1)}+\alpha_{\pi(k+1)}\leq 0.
\end{align*}
It follows that
\begin{align*}
\alpha_{\pi(k-1)}=\alpha_{\pi(k+1)}=0,\quad \text{and}\quad L=R\,,
\end{align*}
since $\alpha_{\pi(k-1)}$ and $\alpha_{\pi(k+1)}$ are non-negative integers.

Again, switching $\alpha_{\pi(k)}$ with either $\alpha_{\pi(k-1)}$ or $\alpha_{\pi(k+1)}$ does not affect the value of the objective function. So after performing a number of switches of necessary, we always obtain a V-shaped optimal permutation.

This shows that we can always construct a V-shaped optimal solution from any optimal solution. Hence, there exists a V-shaped solution to the Wiener Max-QAP.
\end{proof}
