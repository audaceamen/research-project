\chapter{Dynamic programming algorithm for the MaxWiener-Tree problem}\label{Chap4}
%An average research project may contain five chapters, but I didn't plan my work properly
%and then ran out of time. I spent too much time positioning my figures and worrying
%about my preferred typographic style, rather than just using what was provided.
%I wasted days bolding section headings and using double slash line endings, and 
%had to remove them all again. I spent sleepless nights configuring manually numbered lists
%to use the \LaTeX\ environments because I didn't use them from the start or understand
%how to search and replace easily with texmaker.
%
%Everyone has to take some shortcuts
%at some point to meet deadlines. Time did not allow to test model 
%B as well. So I'll skip right ahead and put that under my Future Work section.

In Chapter \ref{Chap3} we used the decomposition property to show that the Wiener Max-QAP has a V-shaped solution. That is, we partially characterized the structure of an optimal solution. In this chapter, following \citep{ccela2011wiener}, we use a dynamic programming method together with the decomposition property to design an algorithmic solution. 
\section{Wiener Max-QAP Algorithm}\label{WienerMaxQAPAlgo}
Consider the Wiener Max-QAP as introduced in Chapter \ref{Chap2}, and its objective function formulated in \eqref{maxwiener}. Let $k\in \{1,2,\ldots,n \}$. Using the decomposition property studied in Chapter \ref{Chap3}, we can construct a ($k+2$)-dimensional version in the following way.

Consider the sequence $\alpha_1,\alpha_2,\ldots,\alpha_k, L, R$ (so trivially $L,R\geq 0$) with $\displaystyle \sum_{i=k+1}^n\alpha_i=L+R$. The 1D-distance is built around the $k$ points $\beta_m, \beta_{m+1}, \ldots, \beta_{m+k-1}$ plus two more points, the first one in $\beta_m$ and the second one in $\beta_{m+k-1}$. So $m$ and $k$ must satisfy $m\geq 1$ and $m+k-1 \leq n$.

In analogy to the proof of Theorem \ref{thm:V-shaped}, we look for the optimal bijective function $\sigma$ that induces an optimal solution to the ($k+2$)-dimensional version which consists of mapping the $k+2$ values
\begin{align*}
L,\quad \alpha_{\sigma(m)},\quad \alpha_{\sigma(m+1)},\quad \ldots,\quad \alpha_{\sigma(m+k-1)},\quad R
\end{align*}
bijectively to the $k+2$ points
\begin{align*}
\beta_{m},\quad \beta_m,\quad \beta_{m+1},\quad \ldots, \quad \beta_{m+k-1},\quad \beta_{m+k-1}\,,
\end{align*} 
subject to the constraints (imposed by the decomposition property) that the values $L$ and $R$ are assigned to the points $\beta_m$ and $\beta_{m+k-1}$, respectively.

More precisely, we want to find a bijective function $\sigma$ from the set $\{m,m+1,\ldots, m+k-1\}$ to the set $\{1,2,\ldots,k\}$ which maximizes the objective function
\begin{align*}
Z:&=\sum_{i=m}^{m+k-1}\sum_{j=m}^{m+k-1}\alpha_{\sigma(i)}\alpha_{\sigma(j)}|\beta_i-\beta_j|\\
&+\sum_{i=m}^{m+k-1}\alpha_{\sigma(i)}L|\beta_i-\beta_m|\\
&+\sum_{i=m}^{m+k-1}\alpha_{\sigma(i)}R|\beta_i-\beta_{m+k-1}|\\
&+\sum_{i=m}^{m+k-1}L\alpha_{\sigma(i)}|\beta_m-\beta_i| + LR|\beta_m-\beta_{m+k-1}|\\
&+\sum_{i=m}^{m+k-1}R\alpha_{\sigma(i)}|\beta_{m+k-1}-\beta_i| + RL|\beta_{m+k-1}-\beta_m|\,,
\end{align*}
which reduces to
\begin{align}\label{state}
Z=&\sum_{i=m}^{m+k-1}\sum_{j=m}^{m+k-1}\alpha_{\sigma(i)}\alpha_{\sigma(j)}|\beta_i-\beta_j|+2LR|\beta_{m+k-1}-\beta_m|\nonumber\\
&+2\sum_{i=m}^{m+k-1}\alpha_{\sigma(i)}L|\beta_i-\beta_m|\\
&+2\sum_{i=m}^{m+k-1}\alpha_{\sigma(i)}R|\beta_{m+k-1}-\beta_i| \nonumber\,.
\end{align}
Since $Z$ depends on the parameters $k,m,L$ and $R$, we use ($k,m,L,R$) and $Z(k,m,L,R)$ to specify a state in the program and the maximum objective value in this state, respectively.

A state ($k,m,L,R$) can be interpreted as follows: $\alpha_{k+1}, \alpha_{k+2}, \ldots, \alpha_{n}$ have already been assigned to the ($n-k$) points $\beta_1, \beta_2, \ldots, \beta_{m-1}, \beta_{m+k}, \beta_{m+k+1}, \ldots, \beta_n$ in such a way that the total weight on $\beta_1, \beta_2, \ldots, \beta_{m-1}$ is $L$ and the total weight on $\beta_{m+k}, \beta_{m+k+1}, \ldots, \beta_n$ is $R$. The remaining weights $\alpha_{1}, \alpha_{2}, \ldots, \alpha_{k}$ are still to be assigned.

Clearly, the parameters $k,m,L$ and $R$ meet the following restrictions
\begin{align}\label{cond}
1\leq k \leq n, \quad \text{and} \quad 1\leq m \leq n-k+1\,.
\end{align}

\begin{rem}
For $k=n$, we have $m=1$, $L=R=0$, $\sigma=\pi$ and $\displaystyle Z=\sum_{i=1}^{n}\sum_{j=1}^{n}\alpha_{\pi(i)}\alpha_{\pi(j)}|\beta_i-\beta_j|$. This means that by increasing $k$, the optimal objective value for the Wiener Max-QAP will be found when $k=n$ and so $m=1$.
\end{rem}

By recursion on $k$ (starting from $k=1$) we find the value of an optimal solution. For $k=1$ we can see from \eqref{state} that $Z=0$ for all the corresponding states. Now let $k\geq 1$. We want to be able to determine the optimal solution of the state ($k+1,m,L,R$) from the optimal solution of prior states.

Denote $\displaystyle M=\sum_{i=1}^k\alpha_i$ and consider the state ($k+1,m,L,R$). Then $\displaystyle L+R=\sum_{i=k+2}^n\alpha_i$. As $\alpha_{k+1}$ is the largest of the $k+1$ values $\alpha_1,\alpha_2,\ldots,\alpha_{k+1}$, the decomposition property together with the V-shaped property tell us that either $\alpha_{k+1}$ will be assigned to $\beta_m$ or assigned to $\beta_{m+(k+1)-1}=\beta_{m+k}$. Therefore, we can find the objective value in this new state as follows:
\begin{itemize}
\item If we assign $\alpha_{k+1}$ to $\beta_m$, then $L$ should be replaced with $L+ \alpha_{k+1}$ and $m$ with $m+1$ in the state ($k,m,L,R$). Making use of the decomposition property, we find that the objective value in this configuration will be
\begin{align*}
Z_1:=&\Big((L+\alpha_{k+1})M|\beta_m-\beta_{m+1}|+(L+\alpha_{k+1})R|\beta_m-\beta_{m+1}|\Big)\\
&+\Big(M(L+\alpha_{k+1})|\beta_{m+1}-\beta_m|\Big)\\
&+\Big(R(L+\alpha_{k+1})|\beta_{m+1}-\beta_m|\Big)\\
&+Z(k,m+1,L+\alpha_{k+1},R)\,,
\end{align*}
which simplifies to
\begin{align}\label{z1}
Z_1=2(L+\alpha_{k+1})(M+R)|\beta_{m+1}-\beta_m|+Z(k,m+1,L+\alpha_{k+1},R)\,.
\end{align}

\item If $\alpha_{k+1}$ is assigned to $\beta_{m+k}$, then only $R$ should be replaced with $R+ \alpha_{k+1}$ in the state ($k,m,L,R$). The optimal objective value in this configuration will be
\begin{align}\label{z2}
Z_2:=2(R+\alpha_{k+1})(L+M)|\beta_{m+k}-\beta_{m+k-1}|+Z(k,m,L,R+\alpha_{k+1})\,.
\end{align}
\end{itemize}
Therefore, the largest possible objective value in the state ($k+1,m,L,R$) will be given by
\begin{align*}
Z(k+1,m,L,R)=\max(Z_1,Z_2),
\end{align*}
with $Z_1,Z_2$ as defined in \eqref{z1} and \eqref{z2}, respectively.

Hence, in this fashion, we can determine all the values $Z(k,m,L,R)$. This is a dynamic programming approach based on one starting state and a recursive formula where a sub-solution of the problem is constructed from previously found ones. In order to solve our original $n$-dimensional problem, we define a collection of sub-problems with a key property that allows them to be solved in a recursive step.

The value $Z(n,1,0,0)$ in the last state $(n,1,0,0)$ of the program corresponds to the optimal solution to the Wiener Max-QAP.

We now summarise the construction by giving a pseudo-code.

\noindent\rule[0.5ex]{\linewidth}{1pt}
\begin{center}
\textbf{Wiener Max-QAP Algorithm}
\end{center}
\noindent\rule[0.5ex]{\linewidth}{0.5pt}	
\textbf{Input:} Two underlying sequences:
\begin{itemize}
\item $\alpha_1\leq \alpha_2\leq \cdots\leq \alpha_n$, where the $\alpha_i$ are non-negative integers,
\item $\beta_1\leq \beta_2\leq \cdots\leq \beta_n$, where the $\beta_i$ are integers.
\end{itemize}

\textbf{Output:} Maximum value of the objective function in \eqref{maxwiener}

\noindent\rule[0.5ex]{\linewidth}{0.5pt}	

define \textbf{Function Z}(list $A:=(\alpha_1,\alpha_2,\ldots,\alpha_n)$, list $B:=(\beta_1,\beta_2,\ldots,\beta_n)$, $k$, $m$, $L$, $R$)
\begin{enumerate}[1.]

\item \quad \textbf{if} $k=1$ \textbf{then return} $0$; 

\item \quad \textbf{if} $2 \leq k \leq n$ \textbf{then}

\item \quad \quad Set $\displaystyle M:=\sum_{i=1}^{k-1} \alpha_i$; \quad $\displaystyle S:=\sum_{i=k+1}^n \alpha_i$; 

\item \quad \quad \textbf{if} $1 \leq m \leq n-k+1$ \textbf{then}

\item \quad \quad \quad \textbf{if} $0\leq L \leq S$ \textbf{then}

\item \quad \quad \quad \quad Set $R:=S-L$; 

\item \quad \quad \quad \quad Set $Z_1:=$\textbf{Z}$(A,B,k-1,m+1,L+\alpha_k,R)+\Big(2(L+\alpha_k)(R+M)|\beta_{m+1}- \beta_m|\Big)$;

\item \quad \quad \quad \quad  Set $Z_2:=$\textbf{Z}$(A,B,k-1,m,L,R+\alpha_k)+\Big(2(M+L)(R+\alpha_k)|\beta_{m+k-1}-\beta_{m+k-2}| \Big)$;

\item \quad \quad \quad \quad \textbf{return} $\max(Z_1,Z_2)$;
\end{enumerate}
\bigbreak
define \textbf{Function WienerMaxQAP}(list $A$, list $B$)
\begin{enumerate}[1.]

\item \quad \textbf{return} \textbf{Z}$(A, B, n,1,0,0) $;
\end{enumerate}
\textbf{END Algorithm.}  

\noindent\rule[0.5ex]{\linewidth}{1pt}

It is clear by construction that our Wiener Max-QAP algorithm is correct. We now evaluate its running time.

\begin{thm}\label{Thm:WienerMaxQAP}
\citep{ccela2011wiener} The Wiener Max-QAP algorithm is pseudo-polynomial with time complexity $\displaystyle O\Bigg(n^2\cdot \sum_i\alpha_i\Bigg)$.
\end{thm}

\begin{proof}
The value of $k$ ranges from $2$ to $n$ and the value of $m$ from $1$ to $n-k+1$. So there are asymptotically $O(n)$ possible values for $k$ and $m$, respectively. The expression $\displaystyle S:=\sum_{i=k+1}^n\alpha_i$ together with the condition $0\leq L \leq S$ show that there are $\displaystyle O\Big(\sum_i\alpha_i\Big)$  possible values of $L$; the values of $R$ are already fully determined by the values of $k$ and $L$. Thus, there are only $\displaystyle O\Bigg(n^2\cdot  \sum_i\alpha_i\Bigg)$ different states in the dynamic program. The remaining instructions in the algorithm (variable assignments and conditional jumps) can be determined in constant time $O(1)$. Also we may assume that arithmetic operations and comparison of two numbers can be done in $O(1)$ since we are not dealing with bit level complexity of algorithms.

Therefore, the running time is equal to the number of states and hence is $\displaystyle O\Bigg(n^2\cdot \sum_i\alpha_i\Bigg)$. All in all, the running time is polynomial in the numerical values of its input $\alpha_1\leq \alpha_2\leq \cdots\leq \alpha_n$ and so by Definition \ref{PseudoPol} the algorithm is pseudo-polynomial.
\end{proof}

\begin{rem}
The Wiener Max-QAP algorithm only gives the optimal objective value. It doesn't give the corresponding optimal permutation. But one can also do this with the same time complexity. We will elaborate on this technique for the MaxWiener-Tree problem.
\end{rem}
            			
\section{Algorithm for the MaxWiener-Tree problem}
The expression of the Wiener index of a tree established in Chapter \ref{Chap2} is given by
\begin{align}\label{indcaterC4}
W(T):=\Bigg(\sum_{i=1}^nl_i\Bigg)^2+(n-1)\sum_{i=1}^n l_i+\frac{1}{2}\sum_{i=1}^n\sum_{j=1}^n (l_i+1)(l_j+1)|j-i|\,.
\end{align}

Let us consider a caterpillar $T$ with degree sequence $(d_1,\ldots,d_r)$. Let $v_1,v_2,\ldots,v_n$ denote the ordered backbone vertices of $T$. The non-leaves (vertices with degree $\geq 2$) are called \textit{internal vertices}. Denote by $d_1,d_2,\ldots,d_n$ the ordered degree sequence of the internal vertices and consequently $d_{n+1},d_{n+2},\ldots,d_r$ the degree sequence of the leaves which means that $d_{n+1}=d_{n+2}=\cdots=d_r=1$. Let us assume $n\geq2$ since $n=1$ is a trivial case.

\begin{defn}
The vertices with degree $d_2,\ldots,d_{n-1}$ are called \textit{inner backbone vertices} and the vertices with degree $d_1$ and $d_n$ are called \textit{outermost backbone vertices}.
\end{defn}

Let $l_i$, $1\leq i \leq n$ be the number of leaves adjacent to vertex $v_i$ on the backbone of $T$. From \eqref{degree-leaf},
\begin{align*}
d_i= \left\{ \begin{array}{rcl}
l_i+1 & \mbox{for} & i=1 \quad \mbox{or} \quad i=n\\
l_i+2 & \mbox{for} & i=2,\ldots,n-1
\end{array}\right.
\end{align*}

Thus, each of the inner backbone vertices $v_i$ with degree $d_i$, $2\leq i \leq n-1$ is adjacent to $l_i=d_i-2$ leaves while each of the outermost backbone vertices $v_i$ with degree $d_i$, $i=1$ or $i=n$, is adjacent to $l_i=d_i-1$ leaves. We cannot directly apply the pseudo-polynomial algorithm developed in Section \ref{Chap1} of this chapter. We need to apply some small cosmetic changes. In particular, the two outermost backbone vertices $v_1$ and $v_n$ have to be specially treated.

Consider the instance of the Wiener Max-QAP defined by the third sum in \eqref{indcaterC4}. We have $\alpha^*_i=l_i+1$ and $\beta_i=i$. We may rewrite $\alpha^*_i$ as follows:
\begin{align}\label{leaf}
\alpha^*_i= \left\{ \begin{array}{rcl}
(d_i-1)+1 & \mbox{for} & i=1 \quad \mbox{or} \quad i=n\\
d_i-1 & \mbox{for} & i=2,\ldots,n-1
\end{array}\right.
\end{align}

Equation \eqref{leaf} then leads us to derive an ($n+2$)-dimensional version of the Wiener Max-QAP in the following way: We construct the 1D-distance matrix by first assigning the values $1,2,\ldots,n$ to the points $\beta_2,\beta_3,\ldots, \beta_{n+1}$, respectively. Next, we assign the value $1$ to the point $\beta_1$ and we set the point $\beta_{n+2}$ to the value $n$. We have
\begin{align*}
\beta_i=\left\{ \begin{array}{rcl}
1 & \mbox{for} & i=1\\
i-1 & \mbox{for} & i=2,\ldots,n+1\\
n & \mbox{for} & i=n+2\\
\end{array}\right.
\end{align*}
Therefore, the product matrix will be built around the $n$ numbers $\alpha_i=d_i-1$ for $1\leq i \leq n$ plus two additional numbers $\alpha_{n+1}=\alpha_{n+2}=1$ subject to the constraints (due to the special treatment of two outermost backbone vertices $v_1$ and $v_n$) that the value $\alpha_{n+1}=1$ has to be assigned to the point $\beta_1=1$, and that the value $\alpha_{n+2}=1$ has to be assigned to the point $\beta_{n+2}=n$. 

We claim that any optimal solution to the MaxWiener-Tree problem is V-shaped.

Indeed, we first observe that the proof of Theorem \ref{thm:V-shaped} remains valid with the restriction that $1$ is assigned to the endpoints since the proof was done for any kind of assignment. Moreover, from the above considerations, a bijective V-shaped function $\pi$ that maps $\{2,3,\ldots,n+1\}$ to $\{1,2,\ldots,n\}$ remains to be V-shaped when extended by $\pi(1)=n+1$ and $\pi(n+2)=n+2$. Hence, we can still use the dynamic program developed in Section \ref{Chap1} of this chapter but it requires the following changes.

Note that in the program we developed in Section \ref{WienerMaxQAPAlgo} we had $1\leq k \leq n$, but here we have $1\leq k \leq n+2$. We distinguish three cases.
\begin{itemize}
\item Case 1: $1\leq k \leq n$.

Trivially, the first value of $m$ must be 2 and since there are $n-k+1$ values of $m$ we must have $2\leq m \leq n-k+2$. Also, both values $L$ and $R$ must be non-zero because of the two outermost backbone vertices $v_1$ and $v_n$ that should be treated specially.

Furthermore, for each $2\leq m \leq n-k+2$,
\begin{align*}
\beta_{m+1}-\beta_m=(m+1-1)-(m-1)=1\,,
\end{align*}
and
\begin{align*}
\beta_{m+k-1}-\beta_{m+k-2}=(m+k-1+1)-(m+k-2+1)=1\,.
\end{align*}
\item Case 2: $k= n+1$.

In the objective function in \eqref{state}, we sum from $i=m$ to $i=m+(n+1)-1=m+n$. But $\alpha_{n+1}$ must be assigned to $\beta_1$; so this condition forces $m$ to 1, $R$ to 1 and $L$ to 0.

Furthermore, for $k= n+1$ and $m=1$, we have
\begin{align*}
\beta_{m+1}-\beta_m=\beta_2-\beta_1=0\,,\quad \text{and}\quad \beta_{m+k-1}-\beta_{m+k-2}=\beta_{n+1}-\beta_n=1\,.
\end{align*}
\item Case 3: $k= n+2$.

In the objective function in \eqref{state}, we sum from $i=m$ to $i=m+(n+2)-1=m+n+1$. But $\alpha_{n+2}$ must be assigned to $\beta_{n+2}$. So, again, this condition forces $m$ to be 1, and both $L$ and $R$ to be 0.

Furthermore, as for the case $k= n+1$, we also have
\begin{align*}
\beta_{m+1}-\beta_m=0\,,\quad \text{and}\quad \beta_{m+k-1}-\beta_{m+k-2}=1\,.
\end{align*}
\end{itemize}
We now have the following algorithm.       			 			

\noindent\rule[0.5ex]{\linewidth}{1pt}
\begin{center}
\textbf{MaxWiener-Tree Algorithm}
\end{center}
\noindent\rule[0.5ex]{\linewidth}{0.5pt}	
\textbf{Input:} Reduced degree sequence $d_1 \leq d_2 \leq \cdots \leq d_n $. 

\textbf{Output:} Maximum value of the Wiener index given in \eqref{indcaterC4}.

\noindent\rule[0.5ex]{\linewidth}{0.5pt}
define \textbf{ 
Function ZTree}(list $A:=(\alpha_1,\alpha_2,\ldots,\alpha_{n+2})$, $k$, $m$, $L$, $R$)
\begin{enumerate}[1.]
\item \quad Set $n:=$size($A$)$-2$;

\item \quad \textbf{if} $k=1$ \textbf{then return} $0$;

\item \quad \textbf{if} $2 \leq k \leq n$ \textbf{then}

\item \quad \quad Set $\displaystyle M:=\sum_{i=1}^{k-1} \alpha_i$; $\displaystyle S:=\sum_{i=k+1}^{n+2} \alpha_i$;

\item \quad \quad \textbf{if} $m=1$ \textbf{then return} $0$;

\item \quad \quad \textbf{if} $2 \leq m \leq n-k+2$ \textbf{then}

\item \quad \quad \quad \textbf{if} $(L=0$ or $R=0)$ \textbf{then return} $0$;

\item \quad \quad \quad \textbf{if} $1 \leq L \leq S-1$ \textbf{then}

\item \quad \quad \quad \quad Set $R:=S-L$;

\item \quad \quad \quad \quad Set $Z_1:=$\textbf{ZTree}$(A,k-1,m+1,L+\alpha_k,R)+ \Big(2(L+\alpha_k)(R+M)\Big)$; \quad \quad \quad 

\item \quad \quad \quad \quad Set $Z_2:=$\textbf{ZTree}$(A,k-1,m,L,R+\alpha_k)+ \Big(2(M+L)(R+\alpha_k)\Big)$;

\item \quad \quad \quad \quad \textbf{return} $\max(Z_1,Z_2)$;

\item \quad \textbf{if} $k=n+1$ \textbf{then}

\item \quad \quad \textbf{if} $m=1$ \textbf{then}

\item \quad \quad \quad \textbf{if} $(L=0$ and $R=1)$ \textbf{then}

\item  \quad \quad \quad \quad Set $Z_1:=$\textbf{ZTree}$(A,k-1,m+1,L+\alpha_k,R)$;

\item \quad \quad \quad \quad Set $Z_2:=$\textbf{ZTree}$(A,k-1,m,L,R+\alpha_k)+ \Big(2(M+L)(R+\alpha_k)\Big)$;

\item \quad \quad \quad \quad \textbf{return} $\max(Z_1,Z_2)$;

\item \quad \quad \textbf{if} $m=2$ \textbf{then return} $0$;

\item \quad \textbf{if} $(k=n+2$ and $m=1$ and $L=0$ and $R=0)$ \textbf{then}

\item \quad \quad Set $Z_1:=$\textbf{ZTree}$(A,k-1,m+1,L+\alpha_k,R)$; $Z_2:=$\textbf{ZTree}$(A,k-1,m,L,R+\alpha_k)$;

\item \quad \quad \textbf{return} $\max(Z_1,Z_2)$;
\end{enumerate}
\bigbreak
define \textbf{Function MaxWienerTree}(list $(d_1,d_2,\ldots,d_n)$)
\begin{enumerate}[1.]
\item \quad Set list $A:=(d_1-1,d_2-1,\ldots,d_{n}-1,1,1)$;

\item \quad \textbf{return} \Bigg($\displaystyle\Bigg(\sum_{i=1}^{n}d_i-2(n-1)\Bigg)^2+(n-1)\Bigg(\sum_{i=1}^{n}d_i-2(n-1)\Bigg)+\frac{1}{2}$\textbf{ZTree}$(A,$~size$(A),1,0,0) $\Bigg);
\end{enumerate} 
\textbf{END Algorithm.}

\noindent\rule[0.5ex]{\linewidth}{1pt}

Note that for $k,L,R$ fixed, all the values \textbf{ZTree}($k,m,L,R$) are the same. Hence, it should also be possible to rewrite this algorithm without the parameter $m$. Consequently, $m$ will not be considered in the evaluation of the running time of the MaxWiener-Tree algorithm. 

As an illustration of this algorithm, we give an implementation using \textit{Sage Math}, which can be accessed at \url{https://bitbucket.org/audaceamen/max-wiener-tree-code/src}

\begin{thm}\label{Thm:MaxWienerTree}
\citep{ccela2011wiener} The MaxWiener-Tree problem can be solved in quadratic time $O(r^2)$, where $r$ denotes the total number of vertices.
\end{thm}

\begin{proof}
By Theorem \ref{Thm:WienerMaxQAP} the running time corresponds to the number of states $(k,L,R)$. Furthermore, asymptotically we have $O(n)$ possible values for $k$ and $\displaystyle O\Big(\sum_i\alpha_i\Big)$ possible values of $L$. Since $n\leq r$, $k$ can then take $O(r)$ possible values. It just remains to show that $\displaystyle O\Big(\sum_i\alpha_i\Big)=O(r)$.

Consider the degree sequence $(d_1,d_2,\ldots,d_n,d_{n+1},\ldots,d_r)$ with $d_{n+1}=d_{n+2}=\cdots=d_r=1$. Since for $1\leq i \leq n$, $\alpha_i=d_i-1$, we have $\displaystyle \sum_{i=1}^nl_i=\sum_{i=1}^n(\alpha_i-1)=\Bigg(\sum_{i=1}^n\alpha_i\Bigg)-n$. But $\displaystyle \Bigg(\sum_{i=1}^nl_i\Bigg)$ is the total number of leaves, $r-n$. So $\displaystyle r=\sum_{i=1}^n\alpha_i$. Now, since $\alpha_{n+1}=\alpha_{n+2}=1$, we then have $\displaystyle \sum_{i=1}^{n+2}\alpha_i=r+2$. Thus by Definition \ref{rate of growth} we get $\displaystyle O\Big(\sum_i\alpha_i\Big)=O(r)$. Hence, there are $O(r^2)$ different states in the dynamic program.

We conclude from Definition \ref{LinearQuadract} that the MaxWiener-Tree algorithm runs in quadratic time.
\end{proof}

\section{Algorithm for finding an optimal permutation}
In this section we derive an algorithm for determining the corresponding optimal permutation that yields the maximum objective value for the MaxWiener-Tree problem. This is done by means of the MaxWiener-Tree algorithm and the V-shaped property.

Consider the input degree sequence $d_1\leq d_2\leq \cdots \leq d_n$ with $d_1\geq 2$. We construct the sequence $(\alpha_1,\alpha_2,\ldots,\alpha_n,\alpha_{n+1},\alpha_{n+2})$ with $\alpha_{n+1}=\alpha_{n+2}=1$ and $\alpha_i=d_i-1$ for $1\leq i \leq n$.

Consider $Z_1$ and $Z_2$ as defined in the MaxWiener-Tree algorithm. Starting with the last state $(k,m,L,R)=(n+2,1,0,0)$, for each state $(k,m,L,R)$ in the program, we compare $Z_1$ with $Z_2$.
\begin{itemize}
\item If $Z_1> Z_2$ then we first store the value $\alpha_k$ in the position $m$ and next move to the state $(k-1,m+1,L+\alpha_k,R)$;
\item If $Z_2< Z_1$ then we first store the value $\alpha_k$ in the position $m+k-1$ and next move to the state $(k-1,m,L,R+\alpha_k)$;
\item If $Z_1=Z_2$ then we have two possibilities: either we store the value $\alpha_k$ in the position $m$ and move to the state $(k-1,m+1,L+\alpha_k,R)$ or we store the value $\alpha_k$ in the position $m+k-1$ and move to the state $(k-1,m,L,R+\alpha_k)$. We opt for the first option in the pseudo-code given in the following.
\end{itemize}

At the end (after testing the case $k=1$) we get the list
\begin{align*}
W:=\Bigg(\Big(\alpha_{n+2},\pi(n+2)\Big),\Big(\alpha_{n+1},\pi(n+1)\Big),\ldots,\Big(\alpha_{2},\pi(2)\Big),\Big(\alpha_{1},\pi(1)\Big)\Bigg)\,,
\end{align*}
in this order with $\pi(n+2)=n+2$ and $\pi(n+1)=1$.

In fact, there is only one possible state for each of the cases $k=n+1$ and $k=n+2$, namely the states $(n+1,1,0,1)$ and $(n+2,1,0,0)$, respectively.

For the state $(n+2,1,0,0)$,
\begin{align*}
Z_1\equiv\textbf{ZTree}(n+1,2,\alpha_{n+2}=1,0)=0\,,
\end{align*}
since the only state corresponding to $k=n+1$ is $(n+1,1,0,1)$. So trivially $Z_2>Z_1$ and since $m+k-1=1+n+2-1=n+2$, then $\alpha_{n+2}$ will be put in position $n+2$.

Likewise, for the state $(n+1,1,0,1)$,
\begin{align*}
Z_2\equiv\textbf{ZTree}(n,1,0,1+\alpha_{n+1}=2)=0\,,
\end{align*}
since the possible states corresponding to $k=n$ are $(n,2,*,*)$. So trivially $Z_1>Z_2$ and so $\alpha_{n+1}$ will be put in position $1$. Since $\alpha_{n+1}$ and $\alpha_{n+2}$ are not constructed from the elements of the degree sequence we remove them from the list $W$ and return the sequence $\Big(\alpha_{\pi(j)}+1\Big)_{1\leq j\leq n}$ in increasing value of $\pi(j)$.

\begin{exa} For the reduced degree sequence $(7,5,3,2)$, the algorithm will internally generate the list $(1,2,4,6,1,1)$ and then returns the list
\begin{align*}
W:=\Big((1, 6), (1, 1), (6, 2), (4, 5), (2, 4), (1, 3)\Big)\,.
\end{align*}
We remove the first two elements $(1,6)$ and $(1,1)$ from $W$ and now return the sequence $(6,1,2,4)$ and therefore $(7, 2, 3, 5)$ will be an optimal permutation of the input $(7,5,3,2)$.
\end{exa}
Here is a pseudo-code.

\noindent\rule[0.5ex]{\linewidth}{1pt}
\begin{center}
\textbf{Algorithm for finding an optimal permutation for the MaxWiener-Tree problem}
\end{center}
\noindent\rule[0.5ex]{\linewidth}{0.5pt}

\textbf{Input:} Reduced degree sequence $d_1 \leq d_2 \leq \cdots \leq d_n $.

\textbf{Output:} An optimal solution (best caterpillar) for the MaxWiener-Tree problem.

\noindent\rule[0.5ex]{\linewidth}{0.5pt}

define \textbf{Function SearchPermutation}(list $(d_1,d_2,\ldots,d_n)$)
\begin{enumerate}[1.]

\item \quad Let $A:=(\alpha_i)_{1\leq i \leq n}$, with $\alpha_i:=d_i-1$;

\item \quad Set $\alpha_{n+1}:=1$; $\alpha_{n+2}:=1$; 

\item \quad Let $W:=(w_i)_{1\leq i \leq n+2}$ be an empty list;

\item \quad Set $k:=n+2$; $m:=1$; $L:=0$; $R:=0$;

\item \quad \textbf{while} $k\geq 1$ \textbf{do}

\item \quad \quad \textbf{if} $Z_1(A,k,m,L,R)\geq Z_2(A,k,m,L,R)$ \textbf{then}

\item \quad \quad \quad Set $w_{n+3-k}:=(\alpha_k,m)$; $m:=m+1$; $L:=L+\alpha_k$; $k:=k-1$;

\item \quad \quad \textbf{else} Set $w_{n+3-k}:=(\alpha_k,m+k-1)$; $R:=R+\alpha_k$; $k:=k-1$;

\item \quad Let $w_i:=\Big(\alpha_{n+3-i},\pi(n+3-i)\Big)$;

\item \quad \textbf{return}  $(\alpha_{\pi(j)}+1)_{1\leq j\leq n}$ in increasing value of $\pi(j)$;
\end{enumerate} 
\textbf{END Algorithm.}

\noindent\rule[0.5ex]{\linewidth}{1pt}

\begin{rem}
This algorithm yields a unique caterpillar tree, but generally there is more than one optimal solution. By taking care of the special case where $Z_1=Z_2$ in the algorithm, one can also compute all possible V-shaped optimal solutions.
\end{rem}
As an illustration of this algorithm, we give an implementation using \textit{Sage Math}. It is available at \url{https://bitbucket.org/audaceamen/max-wiener-tree-code/src}

\begin{exa}
Let $(d_1,d_2,\ldots,d_7)=(80,76,60,30,11,6,2)$ be the degree sequence of the internal vertices of a tree. The algorithm yields $(80,30,11,6,2,60,76)$ as optimal solution which means that the caterpillar tree $T_1$ with ordered backbone vertices $(d_{1,1},d_{1,2},\ldots,d_{1,7})=(d_1,d_4,d_5,d_6,d_7,d_3,d_2)$ is an optimal tree that maximizes the Wiener index. Furthermore, it turns out that $T_1$ is the unique optimal tree \citep{schmuck2012greedy}.

From the MaxWiener-Tree algorithm, we obtain that the maximum Wiener index for this degree sequence is equal to $152736$.
\end{exa}

Note that once we have got an optimal permutation, the maximum Wiener index can also be obtained by direct computation.
%%%%%%%

\begin{rem}
For every optimal solution to MaxWiener-Tree problem, the reverse permutation of the corresponding permutation is also an optimal solution. The statement is in fact true in the following generalization: Any permutation (not necessary optimal) and its reverse both induce the same value in the objective function
\begin{align}\label{OBJ:Final4}
\sum_{i=1}^n\sum_{j=1}^n\alpha_{\pi(i)} \alpha_{\pi(j)}|i-j|\,.
\end{align}

In fact, it may also be clear intuitively that any sequence is equivalent to its own reverse (the caterpillar trees with ordered degree sequences $(d_1,d_2,\ldots,d_n)$ and $(d_n,d_{n-1},\ldots,d_1)$ are isomorphic).
\end{rem}

\begin{proof}
Consider $\displaystyle \sum_{i=1}^n\sum_{j=1}^n\alpha_{\pi(i)} \alpha_{\pi(j)}|i-j|$ and let $\pi$ be any permutation of the set $\{1,2,\ldots,n\}$. Without loss of generality, assume $\pi=(1,2,\ldots,n)$. Then $\pi$ induces
\begin{align}\label{normalPerm}
2\sum_{i=1}^n\sum_{j=i+1}^n\alpha_i \alpha_j|i-j|\,,
\end{align}
as value in the objective function.

The reverse permutation of $\pi$ is $(n,n-1,n-2,\ldots,1)$ and it induces
\begin{align}\label{reversePerm}
2\sum_{i=1}^n\sum_{j=i+1}^n\alpha_{n+1-i} \alpha_{n+1-j}|i-j|\,,
\end{align}
as value in the objective function.

Consider Equation \eqref{reversePerm}. Setting $k=n+1-i$, and $l=n+1-j$, we get
\begin{align*}
2\sum_{i=1}^{n}\sum_{j=i+1}^{n}(j-i)\alpha_i\alpha_j\,.
\end{align*}
Hence,
\begin{align*}
\sum_{i=1}^n\sum_{j=1}^n\alpha_{i} \alpha_{j}|i-j|=\sum_{i=1}^n\sum_{j=1}^n\alpha_{n+1-i} \alpha_{n+1-j}|i-j|\,.
\end{align*}

This yields that a permutation of $\{1,2,\ldots,n \}$ and its reverse induce the same value in the objective function \eqref{OBJ:Final4}. As a consequence of this, for any optimal permutation to the MaxWiener-Tree problem, the corresponding reverse permutation is also an optimal permutation.
\end{proof}

\begin{pro}
If the sequence $(\beta_1,\ldots,\beta_n)$ in the Wiener Max-QAP objective function \eqref{maxwiener} is an arithmetic progression then any optimal permutation to \eqref{OBJ:Final4} is also an optimal permutation to Wiener Max-QAP.
\end{pro}

\begin{proof}
Since $\beta_1\leq\ldots \leq \beta_n$ is an arithmetic sequence, there is a constant $r>0$ such that for all $1\leq i \leq n$, $\beta_{i+1}-\beta_i=r$. So $|\beta_{j}-\beta_i|=r|i-j|$ for all $1\leq i,j \leq n$ and therefore
\begin{align*}
\sum_{i=1}^n\sum_{j=1}^n\alpha_{\pi(i)} \alpha_{\pi(j)}|\beta_i-\beta_j|=r\sum_{i=1}^n\sum_{j=1}^n\alpha_{\pi(i)} \alpha_{\pi(j)}|i-j|\,,
\end{align*}

which means that maximizing \eqref{maxwiener} reduces to maximizing \eqref{OBJ:Final4}.
\end{proof}

\begin{pro}
If two sequences $(\beta_{1,i})_{1\leq i \leq n}$ and $(\beta_{2,j})_{1\leq j \leq n}$ are related by an affine transformation then the optimal permutations for the general case \eqref{maxwiener} with respect to each sequence are the same.
\end{pro}

\begin{proof}
There exist some constants $a$ and $b$ such that $\beta_{2,i} = a \beta_{1,i} + b$ for all ${1\leq i \leq n}$. So
\begin{align*}
|\beta_{2,i}-\beta_{2,j}|=|a||\beta_{1,i}-\beta_{1,j}|\,
\end{align*}
and therefore,
\begin{align*}
\sum_{i=1}^n\sum_{j=1}^n\alpha_{\pi(i)} \alpha_{\pi(j)}|\beta_{2,i}-\beta_{2,j}|=|a|\sum_{i=1}^n\sum_{j=1}^n\alpha_{\pi(i)} \alpha_{\pi(j)}|\beta_{1,i}-\beta_{1,j}|\,,
\end{align*}

which means that any permutation that maximizes \eqref{maxwiener} with respect to $(\beta_{1,i})_{1\leq i \leq n}$ also maximizes \eqref{maxwiener} with respect to $(\beta_{2,j})_{1\leq j \leq n}$ and the corresponding maxima are related by a factor $|a|$.
\end{proof}


%Some research projects may have 3, 5 or 6 chapters. This is just an example. 
%More importantly, do you have at close to 30 pages?  
%Luck has nothing to do with it. Use the techniques suggested for
%writing your research project.
%
%Now you're demonstrating pure talent and newly acquired skills. 
%Perhaps some persistence. Definitely some inspiration. What was that about perspiration? 
%Some team work helps, so every now and then why not browse your friends' research project and provide
%some constructive feedback?