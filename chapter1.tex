\chapter{Introduction}\label{Chap1}
%Explain the context of your research project topic, so that the
%topic itself appears motivated, natural and important.

%Paragraphs are separated by blank lines in the \LaTeX\ code, 
%and the line spacing, paragraph indentation,
%and paragraph spacing are set in the preamble for you, 
%according to AIMS house style.

%This is a textual citation \citet{shannon44}. And this is a parenthetical citation \citep{shannon44}. You %probably want to use the latter more often.

In Assignment Problems (APs) one wants to find an optimal and efficient way to assign objects of a given set to objects of another given set. In most cases, the problem consists in optimizing (maximizing or minimizing) an objective function over all possible assignments. Depending on the form of the function, one may have a linear, quadratic, or higher dimensional assignment problem. In this essay, we deal with one special case of the Quadratic Assignment Problem (QAP).

The study of QAPs started in 1957 with \cite{koopmans1957assignment} modelling problems of assigning plants to locations of economic activities. QAPs play a major role in the modern world because of their broad applications: computer manufacturing \citep{grotschel1992discrete}, statistical data analysis \citep{hubert1986assignment}, scheduling \citep{geoffrion1976scheduling} and optimization problems in graphs, just to mention a few. The QAP is a well-known and well-studied problem in the field of combinatorial optimization because it belongs to the class of NP-hard problems.

Chemists are interested in the relationship between the physical properties of organic compounds and the structural arrangement of the atoms in the molecule. One model consists in representing atoms by vertices and their bonds by edges and looking for the \textit{Wiener index} of the chemical graph. The Wiener index is the sum of the distances between all pairs of vertices in a connected graph and was introduced in 1947 by Harry Wiener \citep{wiener1947structural}. In this essay, we focus on the special case where the graph is a tree and resolve the problem of finding a tree which maximizes the Wiener index among all trees with a given degree sequence, the so-called \textit{MaxWiener-Tree} \citep{ccela2011wiener}. This problem turns into a QAP with some special restrictions. It can be described as follows:

Given $n\geq 2$ non-negative weights $a_1,a_2,\ldots,a_n$, arrange them, by giving a permutation
\begin{align*}
\Big(a_{\pi(1)},a_{\pi(2)},\ldots,a_{\pi(n)}\Big)\,,
\end{align*}
in such a way that the weighted sum
\begin{align*}
\sum_{i=1}^n \sum_{j=1}^n a_{\pi(i)}a_{\pi(j)}|i-j|
\end{align*}
is maximized.

The project is divided into five chapters. In the first chapter, we give an overview of trees and introduce the problem of maximizing the Wiener index of trees. Chapter \ref{Chap2} introduces the Quadratic Assignment Problem and discusses the complexity of its special case called the Wiener Maximum Quadratic Assignment Problem (Wiener Max-QAP) as in \citep{ccela2011wiener}. In Chapter \ref{Chap3} we study the combinatorial structure of the Wiener Max-QAP and partially characterize its optimal solutions. Chapter \ref{Chap4} describes the algorithm given by \cite{ccela2011wiener} and derives an algorithm for finding an optimal solution to the problem of maximizing the Wiener index of trees, given the degree sequence. Finally, in Chapter \ref{Chap5} we briefly summarize the work.
 
\section{An overview of trees}
In this section, we start by giving some basic concepts on trees and the Wiener index. For graph-theoretical preliminaries, we refer to \citep{west2001introduction}.
\begin{defn}
Let $G$ be a finite and simple graph with vertex set $V(G)$. The graph $G$ is \textit{connected} if there is a path between any two vertices $v, w \in V (G)$.
\end{defn}

\begin{defn}\label{distancedef}
A \textit{tree} $T$ is a connected graph with no cycles. In other words, a path connecting two vertices in $T$ is unique and simple. A vertex of degree $1$ in $T$ is called a \textit{leaf}.

The \textit{distance between two vertices} $v, w \in V(T )$ is the number of edges on the unique simple path that connects $v$ and $w$ in $T$. It will be denoted by $d(v, w)$. 
\end{defn}

\begin{pro}
(\citep{korte2002combinatorial}, Theorem 2.4, p.17) A tree with $r$ vertices has exactly $r-1$ edges.
\end{pro}

\begin{exa}
The graph in Fig.~\ref{tree} is a tree since it has no cycles and it is connected. It has 9 vertices and 8 edges. Also, the vertices $A,C,E,G,I$ are the 5 leaves. The unique path that connects vertices $D$ and $H$ is $(D\rightarrow B \rightarrow F \rightarrow H)$. So the distance between $D$ and $H$ is $d(D, H)=3$.

\begin{figure}[!h]\centering
\includegraphics[width=0.55\textwidth]{images/Tree.png}
\caption{A tree with 9 vertices}
\label{tree}
\end{figure}
\end{exa}

\begin{defn}
A \textit{degree sequence} of a tree $T$ is a sequence of its vertex degrees.
\end{defn}

\begin{thm}\label{Thm:DegSequence}
(\citep{berge1973graphs}, p.25) Let $D=(d_1,d_2,\ldots,d_r)$ with $d_1\geq d_2 \geq \ldots \geq d_r$ be a sequence of $r\geq 2$ positive integers. Then $D$ is a \textit{degree sequence} of a tree if and only if
\begin{align*}
d_i\geq 1 \quad \text{for all} \quad 1\leq i \leq r \quad \text{and} \quad \sum_{i=1}^rd_i=2(r-1)\,.
\end{align*} 
\end{thm}
For example, the tree in Fig.~\ref{tree} has degree sequence $(3,3,3,2,1,1,1,1,1)$ and $\sum_{i}d_i=16=2(9-1)$.

In this essay, we will frequently encounter one specific type of trees, the so-called \textit{caterpillars}.

\begin{defn}
A \textit{caterpillar tree} or \textit{caterpillar} is a tree with the property that a path remains if all of its leaves are removed. We call the path that is formed by the non-leaves the \textit{backbone} of the caterpillar.
\end{defn}
For example, Fig.~\ref{caterp} shows a caterpillar tree with $14$ leaves and \textit{degree sequence} $(7,6,4,3,2,2,1,1,...,1)$. Note that $\sum_{i}d_i=38=2(20-1)$ confirms Theorem \ref{Thm:DegSequence}.
\begin{figure}[!h]\centering
\includegraphics[width=0.34\textwidth]{images/CATER1.png}
\caption{A caterpillar tree}
\label{caterp}
\end{figure}

Caterpillar trees play a key role in chemical graph theory. They are used for instance to represent the structure of linear hydrocarbons such as alkanes (general formula $C_nH_{2n+2}$). Fig.~\ref{butane} shows the structure of butane, where the hydrogen atoms (H) represent the leaves and the carbon atoms (C) the backbone vertices.
\begin{figure}[!h]\centering
\includegraphics[width=0.23\textwidth]{images/butane.png}
\caption{Butane $C_4H_{10}$}
\label{butane}
\end{figure}

For a caterpillar tree we will not always consider the 1's in the degree sequence even though they are still vertex degrees, and we will speak of a \textit{reduced degree sequence}. In fact, the number of leaves can already be determined from the remaining vertex degrees: denote the degree sequence as $(d_1,d_2,\ldots,d_n,\underbrace{1,1,\ldots,1}_{(r-n)\quad \text{times}})$. Then by Theorem \ref{Thm:DegSequence} we have $\displaystyle \Bigg(\sum_{i=1}^nd_i\Bigg)+(r-n)=2(r-1)$ which implies $\displaystyle r=\Bigg(\sum_{i=1}^nd_i\Bigg)-n+2$ and so the number of leaves is given by
\begin{align*}
(r-n)= \Bigg(\sum_{i=1}^nd_i\Bigg)-2(n-1)\,.
\end{align*}

Different caterpillar trees can have the same degree sequence. A caterpillar tree $T$ with reduced degree sequence $(d_1,d_2,\ldots,d_n)$ can be represented by an \textit{ordered degree sequence} $\displaystyle \Big(d_{\pi(1)},d_{\pi(2)},\ldots,d_{\pi(n)}\Big)$, where  $\pi$ is a permutation of $\{1,2,\ldots,n\}$ and $d_{\pi(j)}$ denotes the degree of the $j$-th vertex on the backbone of $T$ in this order. 

\begin{rem}\label{NumbCaterp}
Given a reduced degree sequence $D=(d_1,d_2,\ldots,d_n)$, $n\geq 2$, we can count the exact number of caterpillar trees with reduced degree sequence $D$. We observe the following:
\begin{itemize}
\item If the degrees $d_i$ are pairwise distinct then there are exactly $n!/2$ non-isomorphic caterpillar trees with reduced degree sequence $D$. 
\item If some degrees are repeated, say $d_1,d_2,\ldots,d_m$, $m<n$, appear respectively $k_1,k_2,\ldots,k_m$ times, $k_j\geq 1$, in $D$, then
\begin{itemize}
\item if at most one $k_i$ is odd, then there are exactly
\begin{align*}
\frac{1}{2}\binom{n}{k_1,k_2,\ldots,k_m}+\displaystyle \frac{1}{2}\binom{[n/2]}{[k_1/2],[k_2/2],\ldots,[k_m/2]}
\end{align*}
non-isomorphic caterpillar trees with reduced degree sequence $D$, where $[l]$ denotes the integer part of $l$;
\item if more than one $k_i$ is odd, then there are exactly
\begin{align*}
\frac{1}{2}\binom{n}{k_1,k_2,\ldots,k_m}
\end{align*}
non-isomorphic caterpillar trees with reduced degree sequence $D$.
\end{itemize}
\end{itemize}
\end{rem}

\begin{exa}
Let $x$ be a positive integer. Consider the reduced degree sequence
\begin{align*}
D=(x+1,x+1,x+1,3)\,.
\end{align*}
\begin{itemize}
\item If $x=2$, then we only have one caterpillar tree whose reduced degree sequence is $(3,3,3,3)$.
\item If $x\neq 2$, then $T_1$ and $T_2$ in Fig. \ref{caterpT1} and \ref{caterpT2} are the only two non-isomorphic caterpillar trees with reduced degree sequence $D$. Note that $\displaystyle \frac{1}{2}\binom{4}{3,1}=2$ confirms Remark \ref{NumbCaterp}.
\end{itemize}
\begin{figure}[!h]\centering
\includegraphics[width=0.37\textwidth]{images/TreeA1.png}
\caption{Caterpillar tree $T_1$ with ordered degree sequence $(x+1,3,x+1,x+1)$}
\label{caterpT1}
\end{figure}

\begin{figure}[!h]\centering
\includegraphics[width=0.37\textwidth]{images/TreeA3.png}
\caption{Caterpillar tree $T_2$ with ordered degree sequence $(3,x+1,x+1,x+1)$}
\label{caterpT2}
\end{figure}
\end{exa}

\section{The problem of maximizing the Wiener index of trees}
The \textit{Wiener index} $W(T)$ of a tree $T$ is defined as
\begin{align}\label{index1}
W(T):=\sum_{\{v,w\}\subseteq V(T)}d(v,w),
\end{align}
where $d(v,w)$ is the distance between two vertices $v,w \in V(T)$ as defined in Definition \ref{distancedef}.

Many results on the Wiener index of trees can be found in \citep{dobrynin2001wiener}. Our main problem can be stated as follows:

\textbf{Problem statement:}
For a given degree sequence $D=(d_1,\ldots,d_r)$, let $\mathbb{T}(D)$ denote the set of all trees $T$ with degree sequence $D$. Find
\begin{align*}
T^*\in \mathbb{T}(D)\quad \text{such that}\quad W(T^*)\geq W(T)\quad \text{for all}\quad  T\in\mathbb{T}(D)\,,
\end{align*}
and compute $W(T^*)$.

This problem is called the \textit{MaxWiener-Tree} problem.

It turns out that the maximizing trees have a special structure:
\begin{thm}\label{Thm:Caterp}
For the MaxWiener-Tree problem, all the maximizing trees $T^*$ are caterpillars.
\end{thm} 

\begin{proof}
See \citep{shi1993average}. A different proof of this result can be found in \citep{wang2008extremal}.
\end{proof}

It follows that to solve the MaxWiener-Tree problem, it is enough to properly understand the structure of caterpillars with a certain degree sequence.

In his paper \citep{wang2009corrigendum}, Wang claims that a complete characterization of the extremal trees (maximizing caterpillar trees) seems impossible, but it may be easier to derive an algorithm that computes at least one such caterpillar tree.

In 2011, \c{C}ela et al. showed that the MaxWiener-Tree problem can be modelled as a special case of what they called Wiener Max-QAP \citep{ccela2011wiener}.

%\begin{figure}[!h]
%% Use "\centering" in floats (figure, table), but if you need to center
%% some text (why?) use "\begin{center}...\end{center}".
%\centering 
%% Figure environments same as 0.8 * \textwidth please
%% That does not necessarily mean the actual picture size,
%% it is a guideline for the environment which could contain
%% 2 or more pictures! Be consistent and follow the guidelines
%% provided in your sources.
%\includegraphics[width=0.8\textwidth]{images/bandwidth-colour.png}
%\caption{Planning community bandwidth sharing costs. 
%  Note caption capitalization.}
%\label{bandwidth} 
%% if you move the label it breaks the reference numbering; 
%% always have it *after* the caption.
%\end{figure}

%Remember how to include code with {\tt verbatim} 
%and to fix the tabs in {\sf python} in a verbatim environment? 
%It may be best to have an `include' command for code, 
%not to have to re-edit it all the time.
%\verbatimtabinput{code/mycode.py}