\chapter{The Wiener Maximum Quadratic Assignment Problem}\label{Chap2}
%When you get stuck, don't panic. 
%The world is unlikely to end just now. 
%Remember you can consult your supervisor, tutor, Nolu, Jan, 
%and Jeff and Barry at agreed times. 

In this chapter, we introduce the so-called \textit{Wiener Max-QAP} as in \citep{ccela2011wiener} and investigate its complexity. We show that the MaxWiener-Tree problem can indeed be modelled as a special case of the Wiener Max-QAP.
\section{The Quadratic Assignment Problem}
An assignment can be regarded as a bijective map $\pi$ between two finite sets $E$ and $F$ of $n\geq 1$ elements. By identifying the sets $E$ and $F$ with $\{1,2,\ldots,n \}$ we can represent an assignment by a \textit{permutation}. A permutation $\pi$ can be described by the sequence
\begin{align*}
\Big(\pi(1), \pi(2), \ldots, \pi(n)\Big)\,,
\end{align*}
which means that $1$, $2$, $\ldots$, $n$ are mapped to $\pi(1)$, $\pi(2)$, $\ldots$, $\pi(n)$, respectively.

Throughout this essay, $\pi$ is a permutation of the set $\{1,2,\ldots,n\}$ unless otherwise indicated.

The QAP as defined in \citep{ccela2011wiener} accepts as input two $n \times n$ square matrices $A=(a_{ij})$ and $B= (b_{ij})$ with real entries, and asks to find a permutation $\pi$ that maximizes (or minimizes) the following \textit{objective function}:
\begin{align} \label{obj}
Z_{\pi}(A,B):=\sum_{i=1}^n \sum_{j=1}^n a_{\pi(i)\pi(j)}b_{ij}\,.
\end{align}
For example, for $n=3$, we have $3!=6$ possible permutations and so $6$ possible values of $Z_{\pi}(A,B)$. The goal is to find the permutation $\pi$ of $\{1,2,3\}$ that makes the value $Z_{\pi}(A,B)$ maximal. A straightforward way to solve this problem is to compute the values of $Z_{\pi}(A,B)$ for all possible permutations, then identify the maximum value and so the corresponding permutation(s). But we are limited in this way of proceeding when the size of the matrices becomes large. Even for moderate values of $n$, say $n=10$, there are $10!\approx 3.7 \times 10^6$ permutations. Therefore, a complete enumeration is impossible.	

The QAP is known as a hard problem, see for instance \citep{burkard2009assignment}. For most instances, we do not know an efficient way to find a solution to a QAP.

\section{The Wiener Max-QAP}
We introduce a special case of the QAP, which we call the Wiener Max-QAP. The definitions listed below are given for this special case.

\begin{defn}
A matrix $A=(a_{ij})\in \mathbb{N}^{n \times n}$ is said to be a \textit{symmetric product matrix} if there are non-negative integers $\alpha_1 \leq \alpha_2 \leq \cdots \leq \alpha_n$ such that $a_{ij}=\alpha_i \times \alpha_j$, for $1\leq i,j \leq n$.
\end{defn}

\begin{defn}
A matrix $B=(b_{ij})\in \mathbb{N}^{n \times n}$ is said to be a \textit{1-D} (one-dimensional) \textit{distance matrix} if there are integers $\beta_1 \leq \beta_2 \leq \cdots \leq \beta_n$ such that $b_{ij}=|\beta_i - \beta_j|$, for all $1\leq i,j \leq n$.
\end{defn}

\begin{defn}\citep{ccela2011wiener}
The \textit{Wiener Max-QAP} is the special case of the QAP in which $A$ is a symmetric product matrix and $B$ is a 1-D distance matrix.
\end{defn}

The aim of the Wiener Max-QAP is to \textit{maximize} the function in \eqref{obj}, which we can now rewrite as
\begin{align}\label{maxwiener} 
Z_{\pi}(A,B)=\sum_{i=1}^n \sum_{j=1}^n \alpha_{\pi(i)} \alpha_{\pi(j)}|\beta_i - \beta_j|\,.
\end{align}

\begin{rem}\label{ordering}
For any permutation $\pi$, if $\pi(i)<\pi(j)$ then $\alpha_{\pi(i)} \leq \alpha_{\pi(j)}$. Also if $i<j$ then $\beta_i \leq \beta_j$.
\end{rem}

We now return to the MaxWiener-Tree problem introduced in Chapter \ref{Chap1}.

\begin{pro}
The Wiener index of a caterpillar tree can be calculated from its ordered degree sequence.
\end{pro}

\begin{proof}
Consider a caterpillar tree $T$. Denote by $v_1,v_2,\ldots,v_n$ the vertices of $T$ ordered along its backbone and $l_i$ the number of leaves adjacent to vertex $v_i$, $i=1,2,\ldots,n$. Label the $l_i$ leaves adjacent to vertex $v_i$ by $v_{i,j}$, $1\leq j \leq l_i$, and let $C_i$ be the set consisting of vertex $v_i$ together with its $l_i$ adjacent leaves. The vertex $v_i$ is called the \textit{center} of the leaves $v_{i,j}$.

See Fig.~\ref{caterpdouble} for an illustration of these notations for the caterpillar tree we already considered in Fig.~\ref{caterp}.
\begin{figure}[!h]\centering
\includegraphics[width=0.77\textwidth]{images/CATER.png}
\caption{A caterpillar tree with its centers and leaves}
\label{caterpdouble}
\end{figure}

The caterpillar in Fig.~\ref{caterpdouble} has $6$ vertices $v_1,\ldots, v_6$ along its backbone and
\begin{align*}
C_1=\{v_1,v_{1,1}\}\,, \quad C_2=\{v_2,v_{2,1}\}\,, \quad C_3=\{v_3,v_{3,1},\ldots,v_{3,5}\}\,, \\
C_4=\{v_4\}\,, \quad C_5=\{v_5,v_{5,1},v_{5,2}\}\,, \quad C_6=\{v_6,v_{6,1},v_{6,2},\ldots,v_{6,5}\}\,.
\end{align*}

Using \eqref{index1}, the Wiener index of $T$ is given by
\begin{align}\label{indcat}
W(T)=\sum_{i=1}^n\Bigg(\sum_{\{v,w\}\subseteq C_i}d(v,w)\Bigg)+ \sum_{i=1}^n\sum_{j=i+1}^n\Bigg(\sum_{v\in C_i,w\in C_j}d(v,w)\Bigg)\,.
\end{align}

Consider $\displaystyle \sum_{v\in C_i,w\in C_j}d(v,w)$. This sum represents the sum of the distances between all vertex pairs with one vertex in $C_i$ and one vertex in $C_j$ ($i\neq j$). It can be written as the sum of 
\begin{itemize}
\item the distances between all pairs of leaves with one leaf in $C_i$ and one leaf in $C_j$ ($i\neq j$),
\item the distances between the center $v_i$ and the leaves in $C_j$,
\item the distances between the center $v_j$ and the leaves in $C_i$, and
\item the distances between the centers $v_i$ and $v_j$.
\end{itemize}

Given two sets $C_i$ and $C_j$, the distance between a leaf in $C_i$ and a leaf in $C_j$ is $|j-i|+2$; the distance between a center $v_i$ and a leaf in $C_j$ is $|j-i|+1$; and the distance between two centers $v_i,v_j$ is $|j-i|$. Thus,
\begin{align}\label{indcat2}
\sum_{v\in C_i,w\in C_j}d(v,w)&=l_il_j(|j-i|+2)+l_j(|j-i|+1)+l_i(|j-i|+1)+|j-i|\\
&=(l_i+1)(l_j+1)|j-i|+(2l_il_j+l_i+l_j) \nonumber\,.
\end{align}

Consider $\displaystyle \sum_{\{v,w\}\subseteq C_i}d(v,w)$. This sum represents the sum of the distances between all vertex pairs inside $C_i$. It can be written as the sum of the distances between all pairs of leaves inside $C_i$ and the distances between the center and its leaves. In other words,
\begin{align*}
\sum_{\{v,w\}\subseteq C_i}d(v,w)=\sum_{j=1}^{l_i}\sum_{k=j+1}^{l_i}d(v_{i,j},v_{i,k})+ \sum_{j=1}^{l_i}d(v_i,v_{i,j})\,.
\end{align*}

For $j\neq k$, we have $d(v_{i,j},v_{i,k})=2$ and $d(v_i,v_{i,j})=1$. Thus,
\begin{align}\label{indcat1}
\sum_{\{v,w\}\in C_i}d(v,w)=2 \binom {l_i}{2}+ l_i\quad =l_i^2.
\end{align}

Substituting \eqref{indcat1} and \eqref{indcat2} in \eqref{indcat}, we get
\begin{align}
W(T)=\sum_{i=1}^nl_i^2+ \sum_{i=1}^n\sum_{j=i+1}^n\Big((l_i+1)(l_j+1)|j-i|+(2l_il_j+l_i+l_j)\Big)\,,
\end{align}

which may be rewritten as
\begin{align*}
W(T)=&\Bigg(\sum_{i=1}^nl_i^2 + \sum_{i=1}^n\sum_{j=i+1}^n2l_il_j\Bigg)+\Bigg(\sum_{i=1}^n\sum_{j=i+1}^n(l_i+l_j)\Bigg)+\frac{1}{2}\Bigg(\sum_{i=1}^n\sum_{j=1}^n (l_i+1)(l_j+1)|j-i|\Bigg)\\
=&\Bigg(\sum_{i=1}^nl_i\Bigg)^2+\Bigg(\sum_{i=1}^n (n-i)l_i+\sum_{j=2}^n (j-1)l_j\Bigg)+\frac{1}{2}\Bigg(\sum_{i=1}^n\sum_{j=1}^n (l_i+1)(l_j+1)|j-i|\Bigg)\,.
\end{align*}

Hence
\begin{align}\label{indcater}
W(T)=\Bigg(\sum_{i=1}^nl_i\Bigg)^2+(n-1)\sum_{i=1}^n l_i+\frac{1}{2}\sum_{i=1}^n\sum_{j=1}^n (l_i+1)(l_j+1)|j-i|\,.
\end{align}

Furthermore, it is straightforward to see that for a caterpillar $T$ with ordered degree sequence $(d_1,d_2,\ldots,d_n)$, we have
\begin{align}\label{degree-leaf}
l_i= \left\{ \begin{array}{rcl}
d_i-1 & \mbox{for} & i=1 \quad \mbox{or} \quad i=n\,,\\
d_i-2 & \mbox{for} & i=2,\ldots,n-1\,.
\end{array}\right.
\end{align}
Inserting these expressions for $l_i$ in \eqref{indcater} gives the Wiener index of $T$ in terms of the degree sequence. This finishes the proof.
\end{proof}

\begin{rem}
Neither the first term, nor the second sum in the expression \eqref{indcater} depend on how the numbers $l_1,\ldots,l_n$ are assigned to the backbone vertices $v_1,\ldots,v_n$.
\end{rem}

Consider the last sum in \eqref{indcater}. Let $\alpha^*_i=l_i+1$ and $\beta_i=i$. Then the last sum in \eqref{indcater} becomes
\begin{align}\label{WienerThirdTerm}
\sum_{i=1}^n\sum_{j=1}^n \alpha^*_i \alpha^*_j |\beta_i-\beta_j|\,.
\end{align}

Maximizing the expression \eqref{WienerThirdTerm} is essentially a Wiener Max-QAP. Therefore, finding a maximizing caterpillar for an instance of the MaxWiener-Tree problem with an explicitly specified backbone $v_1,\ldots,v_n$ and sequence $(l_1,\ldots,l_n)$ of number of leaves is essentially identical to a Wiener Max-QAP.

\section{Wiener Max-QAP complexity}
\textbf{Running time of algorithms}

In this section, we list some definitions and theorems regarding the running time of algorithms. The definitions, the proofs of the propositions and theorems listed can be found in \citep{korte2002combinatorial}.

\begin{defn}
An \textit{algorithm} consists of a set of valid inputs called \textit{instances} and a sequence of instructions each of which can be decomposed into \textit{elementary steps} (sub-operations), such that for each instance the computation of the algorithm is a uniquely defined finite series of sub-operations and produces a certain output. The \textit{input size} denoted \textit{size}($x$) of an instance $x$ with rational data is the total number of bits needed to code the data in binary representation.

Usually one just wants a good upper bound on the number of elementary steps performed, depending on the input size. 
\end{defn}

When dealing with the number of steps required for an algorithm to be performed, one usually does not consider constant terms of moderate size, and speaks of \textit{asymptotic growth}. This is done by means of the following definition.

\begin{defn}\label{rate of growth}
Let $D\subseteq \mathbb{R}$ be a set and $f,g: D \to \mathbb{R}_{+}$ be two functions. We say that $f$ is $O(g)$ (and simply write $f=O(g)$) if there exist $x_0\in D$ and $\alpha>0$ such that for all $x\geq x_0$ we have $f(x)\leq \alpha g(x) $. If $f=O(g)$ and $g=O(f)$, we say that $f$ and $g$ have the same \textit{rate of growth}.

Note that in particular we have $f=O(f)$.
\end{defn}

\begin{defn}
Let $\mathcal{A}$ be an algorithm with input set $X$ and $f:\mathbb{N} \to \mathbb{R}_{+}$ be a function. If there exists a constant $\alpha >0$ such that for each input $x\in X$ the algorithm $\mathcal{A}$ terminates its computation after at most $\alpha f(\text{size}(x))$ sub-operations, then we say that $\mathcal{A}$ \textit{runs in $O(f)$ time}. We also say that the \textit{running time} (or the \textit{time complexity}) of $A$ is at most $O(f)$.
\end{defn}

\begin{defn}\label{LinearQuadract}
An algorithm with rational input and input size $n$ runs in \textit{polynomial time} if there exists a non-negative integer $k$ such that it runs in $O(n^k)$ and all numbers in intermediate computations can be stored with $O(n^k)$ bits. In the cases $k=1$ and $k=2$, we have a \textit{linear time algorithm} and \textit{quadratic time algorithm}, respectively. Polynomial time algorithms are sometimes called \textit{good} or \textit{efficient} algorithms.
\end{defn}

Finding polynomial time algorithms or even linear time algorithms is very desirable in practice.

\textbf{Complexity classes}

\begin{defn}
A \textit{decision problem} $\mathbb{P}$ is a pair $(X,Y)$, where $X$ is the set of instances and $Y$ the set of inputs for which $\mathbb{P}$ returns yes (so $Y\subset X$). The elements of $Y$ and $X \backslash Y$ are called \textit{yes-instances} and \textit{no-instances} of $\mathbb{P}$, respectively. An algorithm for a decision problem $(X,Y)$ is an algorithm computing the function $f: X \to \{0,1\}$ defined by $f(x)=1$ for $x\in Y$ and $f(x)=0$ for $x\in X \backslash Y$.
\end{defn}

\begin{defn}
The class of all decision problems $\mathbb{P}$ for which there is a polynomial time algorithm is denoted by $P$. In other words, to prove that a problem $\mathbb{P}$ is in $P$ one usually describes a polynomial time algorithm for $\mathbb{P}$. The class of all decision problems for which the yes-instances have proofs that can be verified in polynomial time is denoted $NP$. Formally, algorithms and verifications have to be performed by a Turing machine (see \citep{korte2002combinatorial}, Chapter 15).
\end{defn}

\begin{exa}
The decision problem 'Given a graph $G$, is $G$ Hamiltonian?' is NP. In fact, if we answer yes for this problem, we can show a Hamiltonian circuit in the graph as a certificate; and to check whether a given set of edges is a Hamiltonian circuit is clearly possible in polynomial time. This problem is actually more than NP, it is a so-called \textit{NP-complete} problem \citep{korte2002combinatorial}. 
\end{exa}

The abbreviation NP stands for \textit{non-deterministic polynomial-time}.
\begin{pro}
(\citep{korte2002combinatorial}, Proposition 15.14, p.369) A decision problem is NP if and only if it has a polynomial time non-deterministic algorithm.
\end{pro}

\begin{defn}\label{PolyRedu}
Let $\mathbb{P}_1$ and $\mathbb{P}_2=(X,Y)$ be both decision problems. Let $f: X \to \{0,1\}$ be the function computed by an algorithm for $\mathbb{P}_2$. We say that $\mathbb{P}_1$ \textit{polynomially reduces} to $\mathbb{P}_2$ if there exists a polynomial time algorithm for $\mathbb{P}_1$ using the function $f$.

In other words, $\mathbb{P}_1$ can be polynomially reduced to $\mathbb{P}_2$ if and only if, for every instance $I_1$ of $\mathbb{P}_1$, an instance $I_2$ of $\mathbb{P}_2$ can be constructed in polynomial time (with respect to the size of $I_1$), such that $I_2$ is a yes-instance of $\mathbb{P}_2$ if and only if $I_1$ is a yes-instance of $\mathbb{P}_1$ \citep{ccela1998quadratic}.
\end{defn}

\begin{pro}\label{generalizePbm}
(\citep{korte2002combinatorial}, Proposition 15.16, p.370). If a decision problem $\mathbb{P}_1$ polynomially reduces to a decision problem $\mathbb{P}_2$ and there exists a polynomial time algorithm for $\mathbb{P}_2$, then there exists a polynomial time algorithm for $\mathbb{P}_1$.
\end{pro}

We now extend these concepts to optimization problems.

Note that an optimization problem can be naturally related to a decision problem by imposing a bound on the function to be optimized. For instance, the decision version of the MaxWiener-Tree problem can be formulated as follows: Given a degree sequence $D$ of tree and a positive integer $k$, does there exist a tree with degree sequence $D$ such that its Wiener index is at least $k$?

\begin{defn}
An optimization problem (or decision problem) $\mathbb{P}$ is called \textit{NP-hard} if all problems in NP polynomially reduce to $\mathbb{P}$. An \textit{NP-complete} problem is both NP-hard and NP.
\end{defn}

\cite{sahni1976p} showed that the QAP is a NP-hard problem and \cite{ccela2011wiener} proved that even the special case Wiener Max-QAP is also NP-hard.

\begin{thm}\label{WienerHard}
\citep{ccela2011wiener}. The Wiener Max-QAP is NP-hard.
\end{thm}

We will prove Theorem \ref{WienerHard} by showing that it is at least as hard as a well-known NP-complete problem, the so-called PARTITION problem \citep{garey1979guide}.

\textbf{Problem 1} (PARTITION problem): Given a finite set $\{c_1,c_2,\ldots,c_m \}$ of $m\geq 2$ positive integers, does there exist a subset $S$ of the set $\{1,2,\ldots,m\}$ such that $\displaystyle \sum_{j\in S}c_j=\sum_{j\notin S}c_j$?

\begin{proof}of Theorem \ref{WienerHard}. Note that the equation $\displaystyle \sum_{j\in S}c_j=\sum_{j\notin S}c_j$ implies $\displaystyle \sum_{j=1}^mc_j=2\sum_{j\in S}c_j$. Thus $\displaystyle \sum_{j\in S}c_j=\frac{1}{2}\Bigg(\sum_{j=1}^m c_j\Bigg)$.

We obtain the following problem reformulation of Problem \ref{Chap1}:

\textbf{Problem 2}: Given a finite set $\{c_1,c_2,\ldots,c_m \}$ of $m\geq 2$ positive integers such that $\displaystyle \sum_{j=1}^mc_j=2Q$, does there exist a subset $S\subseteq \{1,2,\ldots,m\}$ such that $\displaystyle \sum_{j\in S}c_j=Q$?

Now we show that the Wiener Max-QAP can be used to solve Problem \ref{Chap2}.

Let $m=2k$ ($k\geq 1$), and
\begin{align*}
\alpha_1,\alpha_2,\ldots,\alpha_{2k}
\end{align*}
be an instance of Problem \ref{Chap2}.

Then we construct the following
\begin{align}\label{OrigPb2}
\alpha_1,\alpha_2,\ldots,\alpha_k,\alpha_{k+1},\ldots,\alpha_{2k}\,, \quad \text{and} \quad \beta_i=
\left\{
\begin{array}{lcr}
1 &\quad \text{for} &\quad 1\leq i \leq k \\
2 &\quad \text{for} &\quad k+1\leq i \leq 2k
\end{array}
\right. \,,
\end{align}
as input of the Wiener Max-QAP.

Note that
\begin{align*}
|\beta_i-\beta_j|=
\left\{
\begin{array}{lcr}
0 &\quad \text{if} \quad &1\leq i,j \leq k  \quad \text{or} \quad k+1\leq i,j \leq 2k\\
1 &\quad \text{otherwise}
\end{array}
\right.\,,
\end{align*}

so the objective function in \eqref{maxwiener} is 
\begin{align}\label{ProofWienerHard1}
\sum_{i=1}^k \sum_{j=k+1}^{2k} \alpha_{\pi(i)} \alpha_{\pi(j)}+ \sum_{i=k+1}^{2k} \sum_{j=1}^{k} \alpha_{\pi(i)} \alpha_{\pi(j)} =2\sum_{i=1}^k \sum_{j=k+1}^{2k} \alpha_{\pi(i)} \alpha_{\pi(j)}\,.
\end{align}

Let $J=\{\pi(1),\pi(2),\ldots,\pi(k)\}$. Then the right side of Equation \eqref{ProofWienerHard1} becomes
\begin{align*}
2\sum_{l\in J} \sum_{p\notin J} \alpha_{l} \alpha_{p}=2\Bigg(\sum_{l\in J} \alpha_{l}\Bigg)\Bigg(\sum_{p\notin J}  \alpha_{p}\Bigg)\,.
\end{align*}

Setting $\displaystyle x=\sum_{l\in J} \alpha_{l}$ and using the fact that $\alpha_1,\alpha_2,\ldots,\alpha_{2k}$ is an instance of Problem \ref{Chap2}, we have
\begin{align}\label{ProofWienerHard2}
2x(2Q-x)
\end{align}
as objective function.

But trivially the function \eqref{ProofWienerHard2} is maximized at $x=Q$, i.e. when $\displaystyle x=\sum_{l\in J} \alpha_{l}=Q$ and its maximum value is $2Q^2$.

This shows that we have constructed a yes-instance (an optimal solution) of the given instance \eqref{OrigPb2} of the Wiener Max-QAP with maximum value $2Q^2$ if and only if Problem \ref{Chap2} answers yes for its original instance $\alpha_1,\alpha_2,\ldots,\ldots,\alpha_{2k}$. Also, this shows by Definition \ref{PolyRedu} that Problem \ref{Chap2} polynomially reduces to Wiener Max-QAP.

Thus, by Proposition \ref{generalizePbm} the Wiener Max-QAP generalizes Problem \ref{Chap2} and hence, it is NP-hard.
\end{proof}

We end this chapter with the following definition which we will use in Chapter \ref{Chap4}.
\begin{defn}\label{PseudoPol}
\citep{korte2002combinatorial} Let $\mathbb{P}$ be a decision problem (or an optimization problem) such that each input $x$ is a list of non-negative integers. Let largest($x$) denote the largest of these integers. An algorithm for $\mathbb{P}$ is called \textit{pseudo-polynomial} if its time complexity is bounded by a polynomial in size($x$) and largest($x$).
\end{defn}

%\begin{thm}[Jeff's Washing Theorem]
%\label{thm:jwt}
%If an item of clothing is too big, then washing it makes it bigger;
%but if it is too small, washing it makes it smaller.
%\end{thm}
%
%
%Notice that no Lemmas are required in the proof of Theorem \ref{thm:jwt}.
%
%Use \textbackslash ref for tables, figures, theorems, etc. and \textbackslash eqref for equations.
%
%Use \textbackslash ldots for continuation of commas $,\ldots,$ and \textbackslash cdots for continuation of operators $\times\cdots\times$.